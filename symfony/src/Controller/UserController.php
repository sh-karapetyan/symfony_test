<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    public function create(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $data = [
            'companyName' => $request->request->get('companyName'),
            'postalCode' => $request->request->get('postalCode'),
            'subscriptionPlan' => $request->request->get('subscriptionPlan'),
            'userPassword' => $request->request->get('userPassword'),
            'userEmail' => $request->request->get('userEmail')
        ];
        $validator = Validation::createValidator();
        $constraint = new Assert\Collection([
            'companyName' => new Assert\Length(['min' => 1]),
            'postalCode' => new Assert\Length(['min' => 1]),
            'subscriptionPlan' => new Assert\Length(['min' => 1]),
            'userPassword' => new Assert\Length(['min' => 1]),
            'userEmail' => new Assert\Email()
        ]);
        $violations = $validator->validate($data, $constraint);

        if ($violations->count() > 0) {
            return new JsonResponse(["error" => (string)$violations], 500);
        }

        $companyName = $data['companyName'];
        $postalCode = $data['postalCode'];
        $subscriptionPlan = $data['subscriptionPlan'];
        $userPassword = $data['userPassword'];
        $userEmail = $data['userEmail'];
        $userRole = $request->request->get('userRole');

        $user = new User();
        $user
            ->setCompanyName($companyName)
            ->setPostalCode($postalCode)
            ->setSubscriptionPlan($subscriptionPlan)
            ->setPassword($userPassword)
            ->setEmail($userEmail)
            ->setRoles($userRole)
            ->onPrePersist()
        ;

        $password = $passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($password);

        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
        } catch (\Exception $e) {
            return new JsonResponse(["error" => $e->getMessage()], 500);
        }
        return new JsonResponse(["success" => $user->getUsername(). " has been registered!"], 200);
    }

    public function delete(Request $request)
    {
        try {
            $repository = $this->getDoctrine()->getRepository(User::class);
            $email = $request->request->get('email');
            $userData = $repository->findOneBy([
                'email' => $email,
            ]);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userData);
            $entityManager->flush();
            return new Response(sprintf('%s successfully removed.', $email));
        } catch (\Exception $e) {
            return new JsonResponse(["error" => "Company not found!"], 500);
        }
    }

    public function edit(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        try {
            $data = [];
            $validateData = [];

            if ($request->request->get('userEmail')) {
                $repository = $this->getDoctrine()->getRepository(User::class);
                $email = $request->request->get('userEmail');
                $user = $repository->findOneBy([
                    'email' => $email,
                ]);

                if (!$user) {
                    return new JsonResponse(["error" => 'Company not exists'], 500);
                }
            } else {
                return new JsonResponse(["error" => 'Please set Company Email'], 500);
            }

            if ($request->request->get('companyName')) {
                $data['companyName'] = $request->request->get('companyName');
                $validateData['companyName'] = new Assert\Length(['min' => 1]);
                $user->setCompanyName($data['companyName']);
            }

            if ($request->request->get('postalCode')) {
                $data['postalCode'] = $request->request->get('postalCode');
                $validateData['postalCode'] = new Assert\Length(['min' => 1]);
                $user->setPostalCode($data['postalCode']);
            }

            if ($request->request->get('subscriptionPlan')) {
                $data['subscriptionPlan'] = $request->request->get('subscriptionPlan');
                $validateData['subscriptionPlan'] = new Assert\Length(['min' => 1]);
                $user->setSubscriptionPlan($data['subscriptionPlan']);
            }

            if ($request->request->get('userPassword')) {
                $data['userPassword'] = $request->request->get('userPassword');
                $validateData['userPassword'] = new Assert\Length(['min' => 1]);
                $user->setPassword($data['userPassword']);

                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
            }

            if (!empty($validateData)) {
                $validator = Validation::createValidator();
                $constraint = new Assert\Collection($validateData);
                $violations = $validator->validate($data, $constraint);

                if ($violations->count() > 0) {
                    return new JsonResponse(["error" => (string)$violations], 500);
                }
            }

            $userRole = $request->request->get('userRole');

            if ($userRole) {
                $user->setRoles($userRole);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        } catch (\Exception $e) {
            return new JsonResponse(["error" => $e->getMessage()], 500);
        }
        return new Response(sprintf('%s already upadeted!', $email));
    }

    public function view_list()
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $users = $repository->findAll();

        if (!$users) {
            return new JsonResponse(["error" => 'No company exists'], 500);
        }

        $data = [];
        foreach ($users as $user) {
            $data[] = [
                'companyName' => $user->getCompanyName(),
                'postalCode' => $user->getPostalCode(),
                'subscriptionPlan' => $user->getSubscriptionPlan(),
                'Role' => $user->getRoles(),
                'email' => $user->getEmail()
            ];
        }

        return new JsonResponse($data, 200);
    }
}