import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from "../views/Auth";
import Login from "../views/Login";
import Register from "../views/Register";
import Dashboard from "../views/Dashboard";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'auth',
        component: Auth,
        children: [
            {
                path: '/login',
                name: 'login',
                component: Login
            },
            {
                path: '/register',
                name: 'register',
                component: Register
            }
        ]
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
    if(to.meta.title)
        document.title = to.meta.title;
    const path = [
        '/login',
        '/register',
    ];
    if (to.name === 'auth' && localStorage.getItem('token')) {
        next('/dashboard')
    }
    if (path.includes(to.path) || localStorage.getItem('token')){
        return next();
    }
    next('/login')
});

export default router
