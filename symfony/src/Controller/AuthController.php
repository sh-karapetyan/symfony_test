<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Entity\Auth;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class AuthController extends AbstractController
{
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $data = [
            'companyName' => $request->request->get('companyName'),
            'postalCode' => $request->request->get('postalCode'),
            'subscriptionPlan' => $request->request->get('subscriptionPlan'),
            'userPassword' => $request->request->get('userPassword'),
            'userEmail' => $request->request->get('userEmail')
        ];

        $validator = Validation::createValidator();
        $constraint = new Assert\Collection([
            'companyName' => new Assert\Length(['min' => 1]),
            'postalCode' => new Assert\Length(['min' => 1]),
            'subscriptionPlan' => new Assert\Length(['min' => 1]),
            'userPassword' => new Assert\Length(['min' => 1]),
            'userEmail' => new Assert\Email(),
        ]);
        $violations = $validator->validate($data, $constraint);

        if ($violations->count() > 0) {
            return new JsonResponse(["error" => (string)$violations], 500);
        }

        $user = new User();
        $user
            ->setCompanyName($data['companyName'])
            ->setPostalCode($data['postalCode'])
            ->setSubscriptionPlan($data['subscriptionPlan'])
            ->setPassword($data['userPassword'])
            ->setEmail($data['userEmail'])
            ->setRoles($request->request->get('userRole'))
            ->onPrePersist()
        ;

        $password = $passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($password);

        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
        } catch (\Exception $e) {
            return new JsonResponse(["error" => $e->getMessage()], 500);
        }

        return new JsonResponse(["success" => $user->getUsername(). " has been registered!"], 200);
    }

    public function login(Request $request, JWTTokenManagerInterface $JWTManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        try {
            $repository = $this->getDoctrine()->getRepository(User::class);
            $userData = $repository->findOneBy([
                'email' => $request->request->get('username'),
            ]);

            $validPassword = $passwordEncoder->isPasswordValid($userData, $request->request->get('password'));

            if (!$validPassword) {
                return new JsonResponse(["error" => 'Wrong creadentials.'], 500);
            }

            $token =  $JWTManager->create($userData);

            $userInformations = [
                'token' => $token
            ];

            $auth = new Auth();
            $repository = $this->getDoctrine()->getRepository(Auth::class);
            $existAuth = $repository->findOneBy([
                'user' => $userData
            ]);

            $entityManager = $this->getDoctrine()->getManager();

            if ($existAuth) {
                $existAuth
                    ->setToken($token)
                    ->onPreUpdate();
            } else {
                $auth
                    ->setUser($userData)
                    ->setToken($token)
                    ->onPrePersist();

                $entityManager->persist($auth);
            }

            $entityManager->flush();
        } catch (\Exception $e) {
            return new JsonResponse(["error" => $e->getMessage()], 500);
        }

        return $this->json($userInformations);
    }

    public function api()
    {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
    }
}