# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Docker start ###

* docker-compose up -d

### Migration run ###

* docker exec -it symfonytest_php_1 bash
* composer install
* php bin/console doctrine:migrations:migrate
* exit;

Note: symfonytest_php_1 is your php container name. For see your php container name please run -> docker ps
Container name can be different.

### vue URLs ###

* Login: http://172.21.0.6:8080
* Register: http://172.21.0.6:8080/register

Note: if URL not working please re run this -> docker-compose up -d

### Database adminer URL ###

* http://localhost:8080/

### Database credentials ###

* Host(server): mysql
* username: root
* password: root

* DB: symfony_db

