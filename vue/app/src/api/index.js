export default {
    login (payload) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify(payload);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        return fetch("https://localhost/api/login_check", requestOptions)
            .then(response => response.json())
            .then((response) => {
                if (!response.token) {
                    throw new Error('error')
                }
                localStorage.setItem('token', response.token)
            }).catch(() => {
                return 'error'
            })
    },

    register(payload) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

        var urlencoded = new URLSearchParams();
        urlencoded.append("companyName", payload.companyName);
        urlencoded.append("postalCode", payload.postalCode);
        urlencoded.append("userPassword", payload.userPassword);
        urlencoded.append("userEmail", payload.userEmail);
        urlencoded.append("userRole[]", payload.userRole[0]);
        urlencoded.append("userRole[]", payload.userRole[1]);
        urlencoded.append("subscriptionPlan", payload.subscriptionPlan);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
        };

        return fetch("https://localhost/register", requestOptions)
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Invalid credentials')
                }
            }).catch(() => {
            return 'error'
        })
    },

    getUsers() {
        let token = localStorage.getItem('token')
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${token}`);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        return fetch("https://localhost/user/view_list", requestOptions)
            .then(response => response.json())
            .then(result => result)
            .catch(error => error);
    },

    createCompany(payload) {
        let token = localStorage.getItem('token')
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        myHeaders.append("Authorization", `Bearer ${token}`);

        var urlencoded = new URLSearchParams();
        urlencoded.append("companyName", payload.companyName);
        urlencoded.append("postalCode", payload.postalCode);
        urlencoded.append("userPassword", payload.userPassword);
        urlencoded.append("userEmail", payload.userEmail);
        urlencoded.append("userRole[]", payload.userRole[0]);
        urlencoded.append("userRole[]", payload.userRole[1]);
        urlencoded.append("subscriptionPlan", payload.subscriptionPlan);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
        };

        return fetch("https://localhost/user/create", requestOptions)
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Invalid credentials')
                }
            }).catch(() => {
                return 'error'
            })
    },
    edit(companyName, postalCode, subscriptionPlan, userEmail) {
        let token = localStorage.getItem('token')
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        myHeaders.append("Authorization", `Bearer ${token}`);

        var urlencoded = new URLSearchParams();
        urlencoded.append("userEmail", userEmail);
        urlencoded.append("companyName", companyName);
        urlencoded.append("postalCode", postalCode);
        urlencoded.append("subscriptionPlan", subscriptionPlan);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
        };

        return fetch("https://localhost/user/edit", requestOptions)
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Invalid credentials')
                }
            }).catch(() => {
                return 'error'
            })
    },
    deleteCompany(email) {
        let token = localStorage.getItem('token')
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        myHeaders.append("Authorization", `Bearer ${token}`);

        var urlencoded = new URLSearchParams();
        urlencoded.append("email", email);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
        };

        return fetch("https://localhost/user/delete", requestOptions)
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Invalid credentials')
                }
            }).catch(() => {
                return 'error'
            })
    }
}